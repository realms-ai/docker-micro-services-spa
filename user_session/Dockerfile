# Instructions from the app developer
# - you should use the 'ruby' official image, with the alpine 2.4 branch
FROM ruby:2.4.1-alpine

# - this app listens on port 80
EXPOSE 3000 
#  so it will respond to http://localhost:3000 on your computer

# - then it should use alpine package manager to install tini: 'apk add --update -qq'. for updating in quiet mode
# RUN apk add --update -qq alpine-sdk nodejs postgresql-dev postgresql-client 
RUN apk update -qq
RUN apk add alpine-sdk
RUN apk add nodejs
RUN apk add postgresql-dev
RUN apk add postgresql-client
RUN apk add tzdata

# - run in development mode to generate database strucute
RUN apk add graphviz

# - then it should create directory /usr/src/app for app files with 'mkdir -p /usr/src/app'
RUN mkdir -p /usr/src/app 
# RUN mkdir -p /usr/src/gems/cache 

# - Ruby uses a "gemfile & gemfile lock", so it needs to copy in both the files
 WORKDIR /usr/src/app
 COPY Gemfile Gemfile.lock ./

# - then it needs to run 'bundle install' to install dependencies from that file and store in cache path for future references
RUN gem install bundler
# - install bundle for production mode
# RUN bundle install --jobs 20 --retry 5 --without development test
# - install bundle for development mode
RUN bundle install --jobs 20 --retry 5 --without production

# - then it needs to copy in all files from current directory as Volume used as mountpoint for development
COPY . . 

# - IN DEVELOPMENT MODE
# - then it needs to start container with command 'rails server'
# CMD ["rails", "server"]

# - IN PRODUCTION MODE
# - then it needs to start container with command 'RAILS_ENV=production bundle exec puma -C config/puma.rb'
ENV RAILS_ENV=production
# CMD ["rails", "server"]
CMD ["bundle", "exec", "puma", "-C", "config/puma.rb"]

# - SPECIFYING RAILS_ENV in ENVIRONMENT VARIABLES
# - then it needs to start container with command 'bundle exec puma -C config/puma.rb'
# CMD [bundle", "exec", "puma", "-C", "config/puma.rb"]

# - in the end you should be using FROM, RUN, WORKDIR, COPY, EXPOSE, and CMD commands
# - How to create a build and run it on local machine 
# docker build -t user_session:development_0.0.1 .
# docker container run -e POSTGRES_USER=postgres -e POSTGRES_PASSWORD=password -e RAILS_ENV=development OR production -e POSTGRES_HOST=localhost -p 3000:3000 --name user_session user_session:0.0.1 rails db:setup

# docker container run --link psql:postgres -p 3000:3000 --name user_session -d user_session:development_0.0.1

# - First Time Setting up of DB and run migrations
# docker container exec -it user_session rails db:setup

# - To run migrations in future deployment
# docker container exec -it user_session rails db:migrate

