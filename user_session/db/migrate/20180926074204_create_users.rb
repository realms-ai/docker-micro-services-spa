class CreateUsers < ActiveRecord::Migration[5.2]
  def change
    create_table :users do |t|
      t.string :name, null: false
      t.string :email, null: false
      t.string :username, null: false
      t.integer :age
      t.string :gender
      t.string :profile_pic
      t.string :encrypted_password
      t.string :authentication_code
      t.boolean :valid_email, default: false
      t.string :forgot_password_code
      t.integer :role_id, default: 1

      t.timestamps
    end
    add_index :users, :name, unique: false
    add_index :users, :email, unique: true
    add_index :users, :username, unique: true
  end
end
