class CreateTimezones < ActiveRecord::Migration[5.2]
  def change
    create_table :timezones do |t|
      t.string :name, null: false
      t.string :city, null: false
      t.string :utc, null: false
      t.string :db_utc, null: false
      t.references :user, foreign_key: true, null: false, index: true

      t.timestamps
    end
    add_index :timezones, :name
    add_index :timezones, :city
  end
end
