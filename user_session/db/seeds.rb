# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

require 'bcrypt'

users = ['user', 'manager', 'admin']
timezones = (0..47).to_a.map do |n|
    if (n < 23) 
        if n%2 == 0
            (n - (12 + (n / 2))).to_s + ':00'
        else
            (n - (12 + (n / 2))).to_s + ':30'
        end
    else
        if n%2 == 0
            (n - (12 + (n / 2))).to_s + ':30'
        else
            (n - (12 + (n / 2))).to_s + ':00'
        end
    end
end
# Create Users
users.each_with_index do |user,index|
    u = User.create(
        name: user,
        username: user,
        email: "#{user}@toptal-test.in",
        encrypted_password: BCrypt::Password.create(user),
        role_id: index + 1
    )
end

User.all.each do |user|
    (1..5).to_a.each do |n|
        utc = timezones[rand(47)]
        db_utc = utc.include?("-") ? utc.gsub('-','') : '-' + utc
        user.timezones.create(
            name: "#{user.name}_timezone_#{n}",
            city: "#{user.name}_timezone_city_#{n}",
            utc: utc,
            db_utc: db_utc
        )
    end
end