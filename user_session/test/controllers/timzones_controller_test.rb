require 'test_helper'

class TimzonesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @timzone = timzones(:one)
  end

  test "should get index" do
    get timzones_url, as: :json
    assert_response :success
  end

  test "should create timzone" do
    assert_difference('Timzone.count') do
      post timzones_url, params: { timzone: { city: @timzone.city, name: @timzone.name, utc: @timzone.utc } }, as: :json
    end

    assert_response 201
  end

  test "should show timzone" do
    get timzone_url(@timzone), as: :json
    assert_response :success
  end

  test "should update timzone" do
    patch timzone_url(@timzone), params: { timzone: { city: @timzone.city, name: @timzone.name, utc: @timzone.utc } }, as: :json
    assert_response 200
  end

  test "should destroy timzone" do
    assert_difference('Timzone.count', -1) do
      delete timzone_url(@timzone), as: :json
    end

    assert_response 204
  end
end
