Rails.application.routes.draw do
  # resources :timzones
  # resources :users
  post "/signup", to: "sessions#signup"
  post "/login", to: "sessions#login"
  get "/logout", to: "sessions#logout"
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  # post '*path', to: 'application#invalid_route', via: :all
end
