
class User < ApplicationRecord
    # RELATIONSHIPS
    has_many :timezones

    # VALIDATIONS
    validates :name, presence: true
    validates :email, :username, presence: true, uniqueness: true
    validates :email, format: { with: URI::MailTo::EMAIL_REGEXP } 
    validates :encrypted_password, presence: true, on: :create

    private
end
