class Timezone < ApplicationRecord
    belongs_to :user

    # VALIDATIONS
    validates :name, :city, :utc, presence: true
end
