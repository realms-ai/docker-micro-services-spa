class TimezonesController < ApplicationController
  before_action :set_timzone, only: [:show, :update, :destroy]

  # GET /timzones
  # GET /timzones.json
  def index
    @timzones = Timzone.all
  end

  # GET /timzones/1
  # GET /timzones/1.json
  def show
  end

  # POST /timzones
  # POST /timzones.json
  def create
    @timzone = Timzone.new(timzone_params)

    if @timzone.save
      render :show, status: :created, location: @timzone
    else
      render json: @timzone.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /timzones/1
  # PATCH/PUT /timzones/1.json
  def update
    if @timzone.update(timzone_params)
      render :show, status: :ok, location: @timzone
    else
      render json: @timzone.errors, status: :unprocessable_entity
    end
  end

  # DELETE /timzones/1
  # DELETE /timzones/1.json
  def destroy
    @timzone.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_timzone
      @timzone = Timzone.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def timzone_params
      params.require(:timzone).permit(:name, :city, :utc)
    end
end
