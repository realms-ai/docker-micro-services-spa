require 'bcrypt'
class SessionsController < ApplicationController
  # users.password_hash in the database is a :string
  include BCrypt

  before_action :decode_jwt, except: [:signup, :login]
  # POST signup to create new user in the datbase
  def signup
    if(match_password)
      @user = User.new(user_params)
      @user.encrypted_password = @encrypted_password

      if @user.save
        render "users/show", status: :created
      else
        render json: @user.errors, status: :unprocessable_entity
      end
    else
      render json: {message: "Password & Confirm Password doesn't match"}, status: :unprocessable_entity
    end    
  end

  # POST login to authenticate and make a JWT session of user
  def login
    username_email = login_params[:username]
    password = login_params[:password]
    @user = User.where("email = ? OR username = ?", username_email, username_email)
    if @user.empty?
      render json: {Error: "Unauthorized access"}, status: 401
    else
      @user = @user.first
      user_password = BCrypt::Password.new(@user.encrypted_password)
      if user_password == password
        payload = {
          id: @user.id,
          role: @user.role_id
        }
        token = JWT.encode payload, hmac_secret, 'HS256'
        render json: {jwt_token: token}, status: :ok
      else
        render json: {Error: "Unauthorized access"}, status: 401
      end
    end    
  end

  # GET logout to destroy session of user
  def logout 
    # Delete token from REDIS and remove cache from the UI
    render json: {Message: "Session token removed successfully!!!"}, status: :ok
  end

  private
    # Never trust parameters from the scary internet, only allow the white list through.
    def user_params
      params.require(:user).permit(:name, :email, :username)
    end

    def login_params
      params.require(:user).permit(:password, :username)
    end

    def password_params
      params.require(:user).permit(:password, :confirm_password)
    end

    def match_password
      if(password_params["password"] == password_params["confirm_password"])
        @encrypted_password = BCrypt::Password.create(password_params["password"])
      end
    end

    def hmac_secret
      "b81f7e8390f33db1aca97748eccdaafa7e227cd820c5ede38effc930bf2aa50b8fe847156592384d519451fc4f5de25cab0846786c7332f9ebdffb1fd13f60e0"
    end
end
