require 'jwt'
class ApplicationController < ActionController::API
    # providing json web token on login for authentication to user
    include JWT

    def invalid_route
        render json: {Error: "Invalid Route"}, status: 404
    end
    private
        def decode_jwt
            token = request.headers["x-access-token"]
            begin
                decode = JWT.decode token, hmac_secret, true, { algorithm: 'HS256' }
            rescue StandardError => e
                render json: {Error: "Unauthorized access"}, status: 401
            end
            # Check roles and permissions from the decode token to give further access to the user
        end
        def hmac_secret
            "b81f7e8390f33db1aca97748eccdaafa7e227cd820c5ede38effc930bf2aa50b8fe847156592384d519451fc4f5de25cab0846786c7332f9ebdffb1fd13f60e0"
        end
end
