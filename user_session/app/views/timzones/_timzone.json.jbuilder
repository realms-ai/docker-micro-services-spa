json.extract! timzone, :id, :name, :city, :utc, :created_at, :updated_at
json.url timzone_url(timzone, format: :json)
