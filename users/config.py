import os


basedir = os.path.abspath(os.path.dirname(__file__))


class Config(object):
    DEBUG = False
    TESTING = False


class ProductionConfig(Config):
    POSTGRES_USERNAME = os.environ.get("POSTGRES_USERNAME") or "postgres"
    POSTGRES_PASSWORD = os.environ.get("POSTGRES_ENV_POSTGRES_PASSWORD") or "password"
    POSTGRES_HOST = os.environ.get("POSTGRES_PORT_5432_TCP_ADDR") or "localhost"
    POSTGRES_PORT = os.environ.get("POSTGRES_PORT_5432_TCP_PORT") or "5432"
    DB_NAME = os.environ.get("DB_NAME") or "toptal_test"
    SQLALCHEMY_DATABASE_URI = "postgres://" + POSTGRES_USERNAME + ":" + POSTGRES_PASSWORD + "@" + POSTGRES_HOST + "/" + DB_NAME
    print(SQLALCHEMY_DATABASE_URI)
    os.environ["SQLALCHEMY_DATABASE_URI"] = SQLALCHEMY_DATABASE_URI

# "postgres://username:password@localhost/user_session_development" or os.environ.get('APP_DEVELOPMENT_DATABASE_URI'
class DevelopmentConfig(Config):
	DEBUG = True
	SQLALCHEMY_DATABASE_URI = "postgres://postgres:password@localhost/user_session_development" or os.environ.get(
		'APP_DEVELOPMENT_DATABASE_URI'
	)



class TestingConfig(Config):
    TESTING = True
    SQLALCHEMY_DATABASE_URI = "postgres://localhost/user_session_test" or os.environ.get(
        'APP_TESTING_DATABASE_URI'
    )


config = {
    'production': ProductionConfig,
    'development': DevelopmentConfig,
    'testing': TestingConfig,
    'default': DevelopmentConfig,
}
