# Instructions from the app developer
# - you should use the 'python' official image, with the alpine 3.5.6 branch
FROM python:3.5.6-alpine

# - this app listens on port 5000
EXPOSE 80
#  so it will respond to http://localhost:5000 on your computer

# - then it should use alpine package manager to update docker container
RUN apk update -qq
RUN apk add alpine-sdk
RUN apk add postgresql-dev
RUN apk add postgresql-client
RUN apk add libffi-dev
RUN apk add linux-headers

# - then it should create directory /usr/src/app for app files with 'mkdir -p /usr/src/app'
RUN mkdir -p /usr/src/app

# - Create a Working Directory
 WORKDIR /usr/src/app

# - Python uses a "requirements.txt" to install dependencies of framework, so it needs to copy the file in container
 COPY requirements.txt ./

# - then it needs to run 'pip install' to install dependencies from that file and store in cache path for future references
RUN pip install -r requirements.txt

# - then it needs to copy in all files from current directory as Volume used as mountpoint for development
COPY . .

# - IN DEVELOPMENT MODE
# - then it needs to start container with command './manage runserver'
# CMD ["./manage", "runserver"]

# - IN PRODUCTION MODE
# - then it needs to start container with command 'uwsgi --socket 0.0.0.0:5000 --protocol=http -w wsgi:app'
ENV APP_CONFIG='production'
CMD ["uwsgi", "--socket", "0.0.0.0:5000", "--protocol=http", "-w", "wsgi:app"]

# - in the end you should be using FROM, RUN, WORKDIR, COPY, EXPOSE, and CMD commands
# - How to create a build and run it on local machine
# docker build -t users:development_0.0.1 .
# docker container run --link psql:postgres -p 5000:5000 --name users -d users:development_0.0.1

