from .. import ma
from ..models.users import Users


class UsersSchema(ma.ModelSchema):

    class Meta:
        model = Users


user_schema = UsersSchema(exclude=("created_at", "updated_at", "encrypted_password"))
users_schema = UsersSchema(many=True, exclude=("created_at", "updated_at", "encrypted_password"))
