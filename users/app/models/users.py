from .. import db
import datetime
import pdb

class Users(db.Model):

    id = db.Column(db.Integer, primary_key=True)
    # Additional fields
    name = db.Column(db.String, nullable=False)
    email = db.Column(db.String, nullable=False, unique= True)
    username = db.Column(db.String, nullable=False, unique=True)
    age = db.Column(db.Integer)
    gender = db.Column(db.String)
    # profile_pic = db.Column(db.String)
    encrypted_password = db.Column(db.String)
    role_id = db.Column(db.Integer)
    created_at = db.Column(db.DateTime, default = datetime.datetime.now)
    updated_at = db.Column(db.DateTime, default = datetime.datetime.now, onupdate = datetime.datetime.now)

    def __init__(self, params):
        for key, value in params.items():
            # pdb.set_trace()
            self.__dict__[key] =  value


    def __repr__(self):
        return 'Users {}>'.format(self.id, self.name, self.email, self.username, self.age, self.gender)

    def __update__(self, params):
        for key, value in params.items():
            setattr(self,key,value)
