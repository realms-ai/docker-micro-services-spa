from flask import jsonify, request, make_response
from flask_cors import CORS, cross_origin
from . import api
from .. import db#, bcrypt
from ..models.users import Users
from ..schemas.users import user_schema, users_schema
import traceback
import sys
import pdb
import jwt
from sqlalchemy import and_

# import bcrypt

JWT_SECRET_KEY = "b81f7e8390f33db1aca97748eccdaafa7e227cd820c5ede38effc930bf2aa50b8fe847156592384d519451fc4f5de25cab0846786c7332f9ebdffb1fd13f60e0"

def set_user(id):
    return Users.query.get(id)

def user_params(params, role=None):
	allowed_params = ["name", "username", "age", "gender", "profile_pic"]
	if role > 1:
		allowed_params.append("email")
		allowed_params.append("role_id")
	# password_params = ["password", "confirm_password"]
	user_params = {}
	for param in allowed_params:
		if params.get("user[{}]".format(param)) is not None:
			user_params[param] = params.get("user[{}]".format(param))
	# if params.get("user[password]") is not None and params.get("user[password]") == params.get("user[confirm_password]"):
	#     pdb.set_trace()
	#     user_params["encrypted_password"] =  bcrypt.generate_password_hash(params.get("user[password]")).decode('utf-8')
	return user_params

def update_attributes(user, params):
    print("I am in update")
    for key, value in params.items():
        user.__dict__[key] =  value
    return user

def check_authentication(token):
    try:
        decode = jwt.decode(token, JWT_SECRET_KEY, verify=True, algorithms=['HS256'])
        return {'auth': True, 'decode': decode}
    except Exception as error:
        return {'auth': False, 'error': error}


@api.route('/users', methods=['GET'])
@cross_origin()
def index():
	authentication = check_authentication(request.headers.get('x-access-token'))
	if authentication['auth']:
		user_id = authentication['decode']['id']
		role = authentication['decode']['role']
		query = request.args.to_dict()
		filter_condition = []
		filter_con = ""
		if role == 1:
			filter_condition.append(setattr(Users,'id', user_id))
			filter_con += "id = {} ".format(user_id)
		for key, value in query.items():
			if(len(value) < 3):
				filter_condition.append(getattr(Users,key).ilike('%{}%'.format(value)))
				if(len(filter_con) > 0):
					filter_con += ' AND '
				filter_con += "{} ilike '%{}%'".format(key,value)
			else:
				filter_condition.append(getattr(Users,key).ilike('{}%'.format(value)))
				if(len(filter_con) > 0):
					filter_con += ' AND '
				filter_con += "{} ilike '{}%'".format(key, value)
		# pdb.set_trace()
		# print("Filter Condition" + str(filter_condition))
		print("Filter Condition: " + filter_con)
		users = Users.query.filter(filter_con).order_by(Users.created_at.desc()).all()
		user_dump = users_schema.dump(users).data
		return jsonify(user_dump)
	else:
		return make_response(jsonify({"Error": "Access Denied"}), 401)


@api.route('/users/<int:id>', methods=['GET'])
@cross_origin()
def show(id):
	authentication = check_authentication(request.headers.get('x-access-token'))
	if authentication['auth']:
		role = authentication['decode']['role']
		user_id = authentication['decode']['id']
		if id == user_id or role > 1:
			user = set_user(id)
			if user is None:
				return make_response(jsonify({"Error": "Record not found!!!"}), 404)
			else:
				user_dump = user_schema.dump(user).data
				return make_response(jsonify(user_dump), 200)
	return make_response(jsonify({"Error": "Access Denied"}), 401)


@api.route('/users', methods=['POST'])
@cross_origin()
def create():
    authentication = check_authentication(request.headers.get('x-access-token'))
    if authentication['auth'] and authentication['decode']['role'] > 1:
        parameters = request.values
        users_params =  user_params(parameters, 2)
        user = Users(users_params)
        db.session.add(user)
        db.session.commit()
        user_dump = user_schema.dump(user).data
        return make_response(jsonify(user_dump), 201)
    else:
        return make_response(jsonify({"Error": "Access Denied"}), 401)


@api.route('/users/<int:id>', methods=['PUT', 'PATCH'])
@cross_origin()
def update(id):
	authentication = check_authentication(request.headers.get('x-access-token'))
	if authentication['auth']:
		role = authentication['decode']['role']
		user_id = authentication['decode']['id']
		if id == user_id or role > 1:
			user = set_user(id)
			if user is None:
				return make_response(jsonify({"Error": "Record not found!!!"}), 404)
			else:
				parameters = request.values
				# if (role == 1):
				# 	pdb.set_trace()
				# 	parameters.pop('user[role_id]')
				# 	parameters.pos('user[email]')
				user.__update__(user_params(parameters, role))
				db.session.commit()
				user_dump = user_schema.dump(user).data
				return make_response(jsonify(user_dump), 200)
	return make_response(jsonify({"Error": "Access Denied"}), 401)


@api.route('/users/<int:id>', methods=['DELETE'])
@cross_origin()
def delete(id):
	authentication = check_authentication(request.headers.get('x-access-token'))
	# user_id = authentication['decode']['id']
	if authentication['auth'] and authentication['decode']['role'] > 1:
		role = authentication['decode']['role']
		user_id = authentication['decode']['id']
		user = set_user(id)
		if user is None:
			return make_response(jsonify({"Error": "Record not found!!!"}), 404)
		elif user.id == user_id:
			return make_response(jsonify({"Error": "Self Destruct Disabled!!!"}), 403)
		else:
			db.session.delete(user)
			db.session.commit()
			return make_response(jsonify({"Message":"Record Deleted Successfully!!!"}), 200)
	else:
		return make_response(jsonify({"Error": "Access Denied"}), 401)
