from flask import Blueprint, jsonify, request, make_response
import pdb
import python_jwt as pjwt
import jwt
from flask_cors import CORS, cross_origin
# from flask_restful import Api

api = Blueprint('api', __name__, url_prefix='')
CORS(api, resources={r"/users/*": {"origins": "*"}}, expose_headers=['x-access-token', 'Content-Type', 'Accept'], allow_headers=['x-access-token', 'Content-Type', 'Accept'])
# api = Api(blueprint, prefix='')
JWT_SECRET_KEY = "b81f7e8390f33db1aca97748eccdaafa7e227cd820c5ede38effc930bf2aa50b8fe847156592384d519451fc4f5de25cab0846786c7332f9ebdffb1fd13f60e0"

# Import any endpoints here to make them available
#from . import dis_endpoint, dat_endpoint
from .users import *

# Catch Unwanted URL
@api.route('/', defaults={'path': '/users'})
@api.route('/<path:path>')
def catch_all(path):
    return make_response(jsonify({"Error": "Go to path '/users"}), 400)

# Check JWT Token before request but not working with CORS
'''
    @api.before_request
    @cross_origin(expose_headers=['x-access-token', 'Content-Type', 'Accept'], allow_headers=['x-access-token', 'Content-Type', 'Accept'])
    def check_authentication():
        token = request.headers.get("x-access-token")
        try:
            pdb.set_trace()
            decode = jwt.decode(token, JWT_SECRET_KEY, verify=True, algorithms=['HS256'])
            pass
        except Exception as error:
            return make_response(jsonify({"Error": "Invalid Token"}), 401)
'''

# @api.errorhandler(IntegrityError)
# def not_found_error(error):
#     traceback.print_exc()
#     return make_response(jsonify({"Error": "Id not found!!!"}), 403)

@api.errorhandler(Exception)
def internal_error(error):
    print(error)
    traceback.print_exc()
    try:
        if bool(error.__dict__) and error.__dict__['connection_invalidated'] == False:
            return make_response(jsonify({"Error": str(error.__dict__['orig'])}), 403)
        else:
            return make_response(jsonify({"Error": "Server Error!!!"}), 500)
    except Exception as error:
        traceback.print_exc()
        return make_response(jsonify({"Error": "Server Error!!!"}), 500)




