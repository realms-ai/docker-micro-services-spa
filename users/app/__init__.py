from flask import Flask
# from werkzeug.exceptions import HTTPException
# from flask_bcrypt import Bcrypt
from flask_marshmallow import Marshmallow
from flask_sqlalchemy import SQLAlchemy
from config import config


db = SQLAlchemy()
ma = Marshmallow()
# bcrypt = Bcrypt()

def create_app(config_name):
    app = Flask(__name__)
    # app.register_error_handler(HTTPException, lambda e: (str(e), e.code))
    # bcrypt = Bcrypt(app)
    app.config.from_object(config[config_name])

    db.init_app(app)
    ma.init_app(app)
    
    # bcrypt.init_app(app)
    from .api import api as api_blueprint
    app.register_blueprint(api_blueprint, url_prefix='')

    return app
