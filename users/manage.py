#! /usr/bin/env python

import os

from flask_script import Manager
from flask import url_for
import pdb

from app import create_app, db


app = create_app(os.getenv('APP_CONFIG', 'default'))
manager = Manager(app)


# @manager.command
# def list_routes():
#     import urllib
#     output = []
#     for rule in app.url_map.iter_rules():

#         options = {}
#         for arg in rule.arguments:
#             options[arg] = "[{0}]".format(arg)

#         methods = ','.join(rule.methods)
#         url = url_for(rule.endpoint, **options)
#         pdb.set_trace()
#         line = urllib.parse.unquote("{:50s} {:20s} {}".format(rule.endpoint, methods, url))
#         output.append(line)

#     for line in sorted(output):
#         print(line)

@manager.shell
def make_shell_context():
    return dict(app=app, db=db)


if __name__ == '__main__':
    manager.run()


