# To Test User Session or Authentication
newman run Toptal_test.postman_collection.json --folder User_Session -d test_cases/session_test_cases.csv

# To Create Timezones for certain user and check count of total timezones, which user can see
newman run Toptal_test.postman_collection.json -d test_cases/timezone_create_test_cases.csv --folder Login --folder Create --folder Index --folder Show -e environment/Toptal_local.postman_environment.json -r cli,json


# To Update Timezones for certain user on the fly
newman run Toptal_test.postman_collection.json -d test_cases/timezone_update_test_cases.csv --folder Login --folder Update --folder Show -e environment/Toptal_local.postman_environment.json -r cli,json 

# To Destroy Timezones for certain user
newman run Toptal_test.postman_collection.json -d test_cases/timezone_destroy_test_cases.csv --folder Login --folder Destroy --folder Show -e environment/Toptal_local.postman_environment.json -r cli,json

# To Create Users and check count of total users, which user can see
newman run Toptal_test.postman_collection.json -d test_cases/user_create_test_cases.csv --folder Login --folder U_create --folder U_index --folder U_show -e environment/Toptal_local.postman_environment.json -r cli,json

# To Update Users
newman run Toptal_test.postman_collection.json -d test_cases/user_update_test_cases.csv --folder Login --folder U_update --folder U_show -e environment/Toptal_local.postman_environment.json -r cli,json 

# To Delete Users
newman run Toptal_test.postman_collection.json -d test_cases/user_destroy_test_cases.csv --folder Login --folder U_destroy --folder U_show -e environment/Toptal_local.postman_environment.json -r cli,json