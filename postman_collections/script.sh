#!/bin/bash  
# PATH OF SCRIPT FILE
SCRIPTPATH=$(dirname "$SCRIPT")

echo "newman run $SCRIPTPATH/Toptal_test.postman_collection.json --folder User_Session -d $SCRIPTPATH/test_cases/session_test_cases.csv"
# newman run $SCRIPTPATH/Toptal_test.postman_collection.json --folder User_Session -d $SCRIPTPATH/test_cases/session_test_cases.csv

echo "newman run $SCRIPTPATH/Toptal_test.postman_collection.json -d $SCRIPTPATH/test_cases/timezone_create_test_cases.csv --folder Login --folder Create --folder Index --folder Show -e $SCRIPTPATH/environment/Toptal_local.postman_environment.json -r cli,json"