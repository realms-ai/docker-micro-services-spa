import TimezoneController from '../controllers/TimezoneController';
import { Router } from 'express';

/**
 * @class TimezoneRouter
 */
export default class TimezoneRouter {
    public router: Router;

    constructor() {
        this.router = Router();
        this.routes();
    }
    public routes(): void {
        this.router.get('', TimezoneController.index);
        this.router.get('/:id', TimezoneController.set_timezone, TimezoneController.show);
        this.router.post('/', TimezoneController.timezone_parameters, TimezoneController.create);
        this.router.put('/:id', TimezoneController.set_timezone,TimezoneController.timezone_parameters, TimezoneController.update);
        this.router.patch('/:id', TimezoneController.set_timezone,TimezoneController.timezone_parameters, TimezoneController.update);
        this.router.delete('/:id', TimezoneController.set_timezone,TimezoneController.destroy);
    }
}
