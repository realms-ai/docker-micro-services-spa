import * as express from 'express';
import TimezoneRouter from './TimezoneRouter';
import { IServer } from '../interfaces/ServerInterface';
import * as JWT from 'jsonwebtoken';

export default class Routes {
    /**
     * @param  {IServer} server
     * @returns void
     */
    static init(server: IServer): void {
        const router: express.Router = express.Router();
        const secret_key = 'b81f7e8390f33db1aca97748eccdaafa7e227cd820c5ede38effc930bf2aa50b8fe847156592384d519451fc4f5de25cab0846786c7332f9ebdffb1fd13f60e0';
        const jwt_options: any = {
            complete: true,
            algorithm: ['HS256']
        };

        server.app.use('/', router);
        // Authentication for JWT Token
        server.app.use((req, res, next) => {
            const token: any = req.headers['x-access-token'];

            JWT.verify(token, secret_key, jwt_options, (error, decoded) => {
                if (error) {
                    res.status(500).json({
                        error: error.message,
                        errorStack: error.stack
                    });
                    next(error);
                } else {
                    req.body.id = decoded.id;
                    req.body.role = decoded.role;
                    next();
                }
            });
        });
        // timezones
        server.app.use('/timezones', new TimezoneRouter().router);
        // Catch all other URL
        server.app.all('*', (req, res) => {
            res.status(400).json({
                error: "Go to path '/timezones"
            });
        });
        // Error Handler
        server.app.use((err, req, res, next) => {
            // Send error to team on mail
            // Time being stacking error on console
            console.error(err.stack)
            // res.status(500).send('Something broke!')
        })
    }
}
