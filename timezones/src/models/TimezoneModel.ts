import * as connections from '../config/connection';
import * as Sequelize from 'sequelize';
// import { Schema, Document } from 'mongoose';

const sequelize = connections.db;
const Timezone = sequelize.define(
    'timezones',
    {
        name: {
            type: Sequelize.STRING, 
            allowNull: false
        },
        city: {
            type: Sequelize.STRING, 
            allowNull: false
        },
        utc: {
            type: Sequelize.STRING, 
            allowNull: false
        },
        db_utc: {
            type: Sequelize.STRING, 
            allowNull: false
        },
        user_id: {
            type: Sequelize.INTEGER, 
            allowNull: false
        }
    },
    {
        createdAt: 'created_at',
        updatedAt: 'updated_at'
    }
)
/* : Schema = new Schema({
    name: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true
    }
}, {
    collection: 'usermodel',
    versionKey: false
}).pre('save', (next) => {
    // this will run before saving
    if (this._doc) {
        const doc: IUserModel = this._doc;
        const now: Date = new Date();

        if (!doc.createdAt) {
            doc.createdAt = now;
        }
        doc.updatedAt = now;
    }
    next();

    return this;
}); */

export default Timezone;
