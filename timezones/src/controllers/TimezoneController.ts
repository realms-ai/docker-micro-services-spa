import * as express from 'express';
import Timezone from '../models/TimezoneModel';
import sequelize from 'sequelize';
// import { Collection } from 'mongodb';
import Jbuilder from 'jbuilder';
const Op: any = sequelize.Op;

class TimezoneController {
    private timezone: any;
    private timezone_params: any;

    constructor() {
        this.timezone = null;
        this.timezone_params = null;
    }

    // JBUILDER
    public jsonBuilder(input: any): Object {
        const params:any = ['id', 'name', 'city', 'utc', 'user_id'];
        const output:any = {};
        const date:any = new Date();
        const utc:any  = date.getTime() + (date.getTimezoneOffset() * 60000);

        // console.log('UTC', utc);

        params.forEach((key, index) => {
            if (input[key]) {
                output[key] = input[key];
            }
        });
        let offset:any = input['utc'].split(':');

        if (offset[1] && parseInt(offset[1], 16) > 0) {
            offset = offset[0] + '.5';
        } else {
            offset = offset[0];
        }
        console.log(offset);
        const current_timestamp: Date = new Date(utc + (3600000 * offset));

        output['current_timestamp'] = current_timestamp;
        output['current_time'] = current_timestamp.toString().split(' ').splice(4, 6).join(' ');

        return output;
    }

    // Timezone function to find single id from the database
    public set_timezone = (req: express.Request, res: express.Response, next: express.NextFunction):void => {
        const condition: any = { id: req.params.id };

        if (req.body.role !== 3) {
            condition['user_id'] =  req.body.id;
        }
        console.log('condition: ', JSON.stringify(condition));
        Timezone.findAll({
            where: condition,

        })
        .then((data) => {
            this.timezone = data[0];
            if (data.length === 0) {
                res.status(404).json({
                    error: 'Not Found!!!'
                });
            } else if (req.body.role === 3 || req.body.id == this.timezone.user_id) {
                next();
            } else {
                res.status(401).json({
                    error: 'Unauthorized Access!!!'
                });
            }
        })
        .catch((error: Error) => {
            res.status(500).json({
                error: error.message,
                errorStack: error.stack
            });
            next(error);
        });
    }

    /**
     * @param  {express.Request} req
     * @param  {express.Response} res
     * @param  {express.NextFunction} next
     */
    public index = (req: express.Request, res: express.Response, next: express.NextFunction):void => {
        const condition: any = {};

        if (req.body.role !== 3) {
            condition['user_id'] = req.body.id;
        }
        const query: any = req.query;
        const queryKeys: string[] = Object.keys(query);

        if (queryKeys.length > 0) {
            queryKeys.forEach((key) => {
                if (query[key].length < 3) {
                    condition[key] = {
                        [Op.iLike]: `%${query[key]}%`
                    };
                } else {
                    condition[key] = {
                        [Op.iLike]: query[key].concat('%')
                    };
                }
            });
        }
        Timezone
            .findAll({
                where: condition,
                attributes: ['id', 'name', 'city', 'utc', 'user_id',
                [sequelize.literal('CURRENT_TIME AT TIME ZONE timezones.db_utc'), 'current_time'],
                [sequelize.literal('CURRENT_TIMESTAMP AT TIME ZONE timezones.db_utc'), 'current_timestamp']
                ],
                order: [['created_at', 'DESC']],
            })
            .then((data) => {
                // data = this.jsonBuilder(data)
                // debugger;
                res.status(200).json(data );
            })
            .catch((error: Error) => {
                res.status(500).json({
                    error: error.message,
                    errorStack: error.stack
                });
                next(error);
            });
    }

    /**
     * @param  {express.Request} req
     * @param  {express.Response} res
     * @param  {express.NextFunction} next
     */
    public show = (req: express.Request, res: express.Response, next: express.NextFunction):void => {
        const timezone: any = this.timezone;

        res.status(200).json(this.jsonBuilder(timezone));
    }

    /**
     * @param  {express.Request} req
     * @param  {express.Response} res
     * @param  {express.NextFunction} next
     */
    public create = (req: express.Request, res: express.Response, next: express.NextFunction): void => {
        Timezone.create(this.timezone_params)
        .then((data) => {
            res.status(200).json(this.jsonBuilder(data));
        })
        .catch((error: Error) => {
            res.status(500).json({
                error: error.message,
                errorStack: error.stack
            });
            next(error);
        });
    }

    /**
     * @param  {express.Request} req
     * @param  {express.Response} res
     * @param  {express.NextFunction} next
     */
    public update = (req: express.Request, res: express.Response, next: express.NextFunction):void => {
        this.timezone.update(this.timezone_params)
        .then((data) => {
            res.status(200).json(this.jsonBuilder(data))
        })
        .catch((error: Error) => {
            res.status(500).json({
                error: error.message,
                errorStack: error.stack
            });
            next(error);
        });
    }

    public destroy = (req: express.Request, res: express.Response, next: express.NextFunction): void => {
        this.timezone.destroy()
        .then((data) => {
            res.status(200).json({ message: 'Timezone deleted successfully' });
        })
        .catch((error: Error) => {
            res.status(500).json({
                error: error.message,
                errorStack: error.stack
            });
            next(error);
        });
    }

    /**
     * @param  {express.Request} req
     * @param  {express.Response} res
     * @param  {express.NextFunction} next
     */
    public timezone_parameters = (req: express.Request, res: express.Response, next: express.NextFunction): void => {
        const variables_allowed: any = ['name', 'city', 'utc'];
        const params: any = req.body;
        const timezone_param: any = {};

        variables_allowed.forEach((v, i) => {
            timezone_param[v] = params[`timezone[${v}]`]
        });
        if (timezone_param['utc']) {
            let utc:any = timezone_param['utc'];

            if (utc.match(/^(|[+|-])[0-9]{1,2}:[00|30]{2}$/)) {
                if (utc.includes('-')) {
                    timezone_param['utc'] = timezone_param['utc'].replace(/^(\-0){1}/g, '-');
                    utc = utc.replace(/\-/g, '');
                } else {
                    utc = utc.replace(/^(\+0|\+|0){0,1}/g, '');
                    timezone_param['utc'] = utc;
                    utc = '-'.concat(utc);
                }
                timezone_param['db_utc'] = utc;
            } else {
                res.status(404).json({
                    error: 'Format UTC Not Acceptable!!!'
                });
            }
            if (req.method === 'POST') {
                timezone_param['user_id'] = params.id;
            }
            this.timezone_params =  timezone_param;
            next();
        } else {
            res.status(404).json({
                error: 'Format UTC Not Acceptable!!!'
            });
        }
    }
}

export default new TimezoneController();
