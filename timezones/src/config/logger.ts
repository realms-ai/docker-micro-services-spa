
const morgan = require('morgan')
const { combine, timestamp, prettyPrint, colorize, json, simple } = format
const {  } = transports

// const myLogFormatter = (options) => {
//     var formatted = new Date() + ' [' + options.level.toUpperCase() + '] ' + (options.message ? options.message : '') +
//         (options.meta && Object.keys(options.meta).length ? '\n\t' + JSON.stringify(options.meta) : '')
//     return options.colorize ? colors(options.level, formatted) : formatted
// }

const colors = {
    debug: 'grey',
    input: 'cyan',
    info: 'green',
    output: 'blue',
    warn: 'yellow',
    error: 'red'
}

const levels = {
    debug: 0, // FILENAME
    input: 1, // INPUT in REQUEST
    info: 2, // SPECIFIC FUNCTION OR QUERY CALLED
    output: 3, // OUTPUT OF QUERY OR FUNCTION or REQUEST
    warn: 4, // NULL RESULT FROM DB
    error: 5 // ERROR in FUNCTION OR QUERY
}

const logger =  createLogger({
    levels: levels,
    format: combine(
        // colorize({all: true}),
        simple(),
        timestamp(),
        // prettyPrint(),
    ),
    transports: [new transports.Console({
        level: 'error',
        format: combine(
            colorize(),
        //     simple(),
        //     timestamp(),
        //     prettyPrint(),
        ),
    })],           
    })
addColors(colors)


logger.log('debug', 'Logger Testing')
logger.log('input', 'INPUT')
logger.log('info', 'INFORMATION')
logger.log('output', "DATA")
logger.log('warn', 'WARN')
logger.log('error', 'ERROR')


export const Logger = logger