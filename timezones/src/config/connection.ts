import * as Sequelize from 'sequelize';
import { default as config } from '../env/index';

const db_config = config.envConfig.database;
export const db = new Sequelize(
                                db_config.db_name,
                                db_config.username,
                                db_config.password,
                                db_config.additional_info
                            );

// handlers
db
    .authenticate()
    .then(() => {
        console.log('PosgreSQL Connection has been established successfully.');
    })
    .catch(err => {
        console.error('Unable to connect to the database:', err);
    });
