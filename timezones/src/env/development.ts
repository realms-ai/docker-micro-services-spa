// development config

export const envConfig: any = {
    database: {
        db_name: process.env.DB_NAME || 'user_session_development',
        password: process.env.POSTGRES_ENV_POSTGRES_PASSWORD || 'password',
        username: process.env.POSTGRES_USERNAME || 'postgres',
        additional_info: {
            host: process.env.POSTGRES_PORT_5432_TCP_ADDR || 'localhost',
            // dialect: 'mysql'|'sqlite'|'postgres'|'mssql',
            dialect: 'postgres',
            pool: {
                max: 128,
                min: 0,
                acquire: 30000,
                idle: 10000
            },
            // http://docs.sequelizejs.com/manual/tutorial/querying.html#operators
            operatorsAliases: false
        }
    }
};
