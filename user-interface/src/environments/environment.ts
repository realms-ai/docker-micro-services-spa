// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: true,
  // user_session: 'http://localhost:3000/',
  // users: 'http://localhost:5000',
  // timezones: 'http://localhost:3100',
  user_session: 'http://toptal-ruby.sarbat-app.tech/',
  users: 'http://toptal-python.sarbat-app.tech',
  timezones: 'http://toptal-nodejs.sarbat-app.tech',
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
