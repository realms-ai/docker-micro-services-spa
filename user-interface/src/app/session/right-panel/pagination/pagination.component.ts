import { Component, OnInit, Input } from '@angular/core';


@Component({
  selector: 'app-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.scss']
})
export class PaginationComponent implements OnInit {
  @Input('pathname') ppathname: string;

  constructor() { }

  ngOnInit() {
    console.log('Pagination called');
  }

}
