import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse, HttpParams } from '@angular/common/http';
import { Observable, throwError, Subject, BehaviorSubject, of } from 'rxjs';
// import { Observable} from 'rxjs/Rx';
import { catchError, retry, take } from 'rxjs/operators';
// import { timezones } from './timezones';
// import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { CookieService } from 'ngx-cookie-service';
import { environment } from './../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})

export class TimezoneService {
  // private headers: any;
  private url: string;
  // token: string;
  public timezone_object = new Subject<ArrayBuffer>();
  public event = this.timezone_object.asObservable();
  public openModal = new Subject<Object>();
  public modalEvent = this.openModal.asObservable();
  // public timezoneIndex = new Subject<number>();
  // public eventIndex = this.timezoneIndex.asObservable();

  constructor(
    private http: HttpClient,
    private cookieService: CookieService,
  ) {
    this.url = environment.timezones + '/timezones/';
  }

  private generateHeaders(): any {
    const token = this.cookieService.get('x-access-token');
    const headers =  {
      headers: new HttpHeaders({
        'x-access-token': token,
        'Content-Type': 'application/x-www-form-urlencoded',
        'Accept': 'application/json'
      })
    };
    return headers;
  }

  index(query) {
    console.log('Query: ', query);
    return this.http.get(this.url.concat(query || ''), this.generateHeaders())
      .pipe(
        catchError(this.handleError)
      );
  }

  show(id) {
    return this.http.get(this.url + id, this.generateHeaders())
      .pipe(
        catchError(this.handleError)
      )
  }

  create(params) {
    const create_params = new HttpParams({fromObject: params});
    return this.http.post(this.url, create_params, this.generateHeaders())
      .pipe(
        catchError(this.handleError)
      );
  }

  update(id, params) {
    const update_params = new HttpParams({fromObject: params});
    return this.http.put(this.url + id, update_params, this.generateHeaders());
  }

  destroy(id) {
    return this.http.delete(this.url + id, this.generateHeaders());
  }

  append_data(data: ArrayBuffer) {
    // Emitting Observable but didn't work
    this.timezone_object.next(data);
  }

  // updateData(id: number, data: ArrayBuffer) {
  //   this.timezoneIndex.next(id);
  //   this.timezone_object.next(data);
  // }

  openUpdateModal(data: Object, index: number){
    this.openModal.next({operation: 'update', model: 'timezone', data: data,  index: index});
  }


  private handleError(error: HttpErrorResponse) {
    console.error('An error occurred in Timezone Services');
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${JSON.stringify(error.error)}`);
    }
    // return an observable with a user-facing error message
    return throwError(
      'Something bad happened; please try again later.');
  }
}
