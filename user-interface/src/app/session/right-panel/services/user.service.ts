import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse, HttpParams } from '@angular/common/http';
import { Observable, throwError, Subject, BehaviorSubject, of } from 'rxjs';
import { catchError, retry, take } from 'rxjs/operators';
import { CookieService } from 'ngx-cookie-service';
import { environment } from './../../../../environments/environment';


@Injectable({
  providedIn: 'root'
})
export class UserService {
  // headers: any;
  url: string;
  // token: string;
  public userObject = new Subject<ArrayBuffer>();
  public event = this.userObject.asObservable();
  public openModal = new Subject<Object>();
  public modalEvent = this.openModal.asObservable();

  constructor(
    private http: HttpClient,
    private cookieService: CookieService,
  ) {
    // this.headers = {
    //   headers: new HttpHeaders({
    //     'Content-Type': 'application/x-www-form-urlencoded',
    //     'Accept': 'application/json',
    //     'x-access-token': this.token,
    //     'Access-Control-Allow-Origin': '*'
    //   })
    // };
    this.url = environment.users + '/users';
  }

  private generateHeaders(): any {
    const token = this.cookieService.get('x-access-token');
    const headers =  {
      headers: new HttpHeaders({
        'x-access-token': token,
        'Content-Type': 'application/x-www-form-urlencoded',
        'Accept': 'application/json'
      })
    };
    return headers;
  }

  index(query) {
    return this.http.get(this.url.concat(query || ''), this.generateHeaders());
  }

  show(id) {
    return this.http.get(this.url.concat('/', id), this.generateHeaders());
  }

  create(params) {
    const create_params = new HttpParams({fromObject: params});
    return this.http.post(this.url, create_params, this.generateHeaders());
  }

  update(id, params) {
    const update_params = new HttpParams({fromObject: params});
    return this.http.put(this.url.concat('/', id), update_params, this.generateHeaders());
  }

  destroy(id) {
    return this.http.delete(this.url.concat('/', id), this.generateHeaders());
  }

  appendData(data: ArrayBuffer) {
    // Emitting Observable but didn't work
    this.userObject.next(data);
  }

  openUpdateModal(data: Object, index: number){
    this.openModal.next({operation: 'update', model: 'user', data: data,  index: index});
  }

  private handleError(error: HttpErrorResponse) {
    console.error('An error occurred in User Services');
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${JSON.stringify(error.error)}`);
    }
    // return an observable with a user-facing error message
    return throwError(
      'Something bad happened; please try again later.');
  }
}
