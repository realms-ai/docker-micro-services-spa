import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { Observable, interval, timer } from 'rxjs';
import { map, switchMap, takeUntil } from 'rxjs/operators';

// IMPORTING SERVICES
import { TimezoneService } from '../services/timezone.service';
// import { timezones } from '../services/timezones';
import { UserService } from '../services/user.service';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})
export class TableComponent implements OnInit, OnDestroy {
  @Input() tpathname: string;

  public user$: Array<any>;   //ArrayBuffer & Object doesn't work here
  public timezone$: Array<any>;  //ArrayBuffer & Object doesn't work here
  user_theads = ['Name', 'Email', 'Age', 'Gender', 'Role'];
  timezone_theads = ['Name', 'City', 'Current Time', 'Difference'];
  systemGmt: number;
  roles = {1: 'User', 2: 'Manager', 3: 'Admin'};
  searchTerm = null;
  subscription: any;
  clickedSearchTerm = null;


  constructor(
    private user: UserService,
    private timezone: TimezoneService,
  ) {

    // Subscribing OBSERVABLES
    // Subscribed data change in timezone
    this.timezone.timezone_object.subscribe(
      data => {
        if (data['index'] >= 0) {
          this.timezone$[data['index']] = <any>data;
        } else {
          this.timezone$.unshift(<any>data);
        }
        console.log(this.timezone$);

        // USING BLOB BUT didn't work
        // const fileReader = new FileReader();
        // const blob = new Blob([data, this.timezone$]);
        // this.timezone$ = fileReader.readAsArrayBuffer(blob);

        // USING UINT BUT didn't work
        // const tmp = new Uint8Array(data.byteLength + this.timezone$.byteLength);
        // tmp.set(new Uint8Array(data), 0);
        // tmp.set(new Uint8Array(this.timezone$), data.byteLength);
        // const concat_data: ArrayBuffer = tmp.buffer;
        // this.timezone$ = tmp.buffer;
      }
    );

    // Subscribing User Data Changes
    this.user.userObject.subscribe(
      data => {
        if (data['index'] >= 0) {
          this.user$[data['index']] = <any>data;
        } else {
          this.user$.unshift(<any>data);
        }
      });
  }

  ngOnInit() {
    console.log('Table called');
    const ggmt = new Date().toString().split(' ')[5].slice(3);
    const gmt = [parseInt(ggmt.slice(0, 3), 16), parseInt(ggmt.slice(3), 16)];
    const sign = gmt[0] > 0 ? 1 : -1;
    if (gmt[1] > 0){
      this.systemGmt = gmt[0] + (0.5 * sign);
    } else {
      this.systemGmt = gmt[0];
    }
    console.log('system GMT', this.systemGmt);
    this.timezone$ = [];
    if (this.tpathname === 'timezones') {
      // this.timezone.index().subscribe(
      //   data => {
      //     this.timezone$ = <any>data;
      // });

      // INTERVAL FUNCTIONALITY TO CALL A FUNCTION AFTER EVERY 15 SECONDS
      // interval(15000).pipe(
      //   switchMap(() => this.timezone.index()))
      //     .subscribe(
      //       data => {
      //         console.log('Interval called');
      //         this.timezone$ = <any>data;
      //     });

      // TIMER FUNCTIONALITY TO CALL A FUNCTION AFTER EVERY 15 SECONDS BUT START WITH 0 second
      this.subscription = timer(0, 15000).pipe(
        switchMap(() => this.timezone.index(this.clickedSearchTerm ? `?name=${this.clickedSearchTerm}` : this.clickedSearchTerm)))
        .subscribe(
            data => {
              console.log('Search Term: ', this.clickedSearchTerm);
              console.log('Timer called');
              this.timezone$ = <any>data;
        });
    } else {
      this.user.index(this.clickedSearchTerm ? `?name=${this.clickedSearchTerm}` : this.clickedSearchTerm).subscribe(
        data => this.user$ = <any>data
      );
    }
  }

  ngOnDestroy() {
    // Unsubscribe services in Destroy
    console.log('Table component destroyed');
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

  getTimezoneGmt(gmt: string) {
    const ttgmt = gmt.split(':');
    const tgmt = [parseInt(ttgmt[0], 16), parseInt(ttgmt[1], 16)];
    const tsign = tgmt[0] > 0 ? 1 : -1;
    const igmt = tgmt[1] > 0 ? tgmt[0] + (0.5 * tsign) : tgmt[0];
    return igmt;
  }

  edit_timezone(data: Object, index: number) {
    console.log('edit_timezone called');
    this.timezone.openUpdateModal(data, index);
  }

  delete_timezone(id: number, index: number){
    console.log('delete_timezone called');
    const boolean: boolean = confirm('Are you sure?');
    if (boolean) {
      this.timezone.destroy(id).subscribe(
        data => this.timezone$.splice(index, 1)
      );
    }
  }

  edit_user(data: Object, index: number) {
    console.log('edit_user called');
    this.user.openUpdateModal(data, index);
  }

  delete_user(id: number, index: number){
    console.log('delete_user called');
    const boolean: boolean = confirm('Are you sure?');
    if (boolean) {
      this.user.destroy(id).subscribe(
        data => this.user$.splice(index, 1),
        error => {
          console.log('Delete User Error', JSON.stringify(error));
          if (error.status <= 500) {
            alert('Error Occured \n' + error.error.Error);
          } else {
            alert('Error Occured \n Please try again later!!!');
          }
        }
      );
    }
  }

  searchChanged(term: string) {
    console.log('Outside Term: ', term);
    // if (this.tpathname === 'timezones') {
      const url = this.tpathname === 'timezones' ? this.timezone : this.user;
      let output: Array<any>;
      if (term) {
        this.clickedSearchTerm = term;
        console.log('Term: ', term);
        url.index(`?name=${this.clickedSearchTerm}`).subscribe(
          data => {
            output = <any>data;
            this.tpathname === 'timezones' ? this.timezone$ = output : this.user$ = output;
        });
      } else {
        this.clickedSearchTerm = null;
        url.index(null).subscribe(
          data => {
            output = <any>data;
            this.tpathname === 'timezones' ? this.timezone$ = output : this.user$ = output;
        });
      }
    // }
  }
}
