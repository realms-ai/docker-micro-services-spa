import { Component, OnInit, Input, AfterViewInit, ViewChild, Output, EventEmitter, ElementRef, TemplateRef } from '@angular/core';
import { Observable } from 'rxjs';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

// IMPORTING SERVICES
import { TimezoneService } from '../services/timezone.service';
import { UserService } from '../services/user.service';

// IMPORTING MODEL VARIABLES FROM THE TABLE COMPONENT
// import { TableComponent } from '../table/table.component'

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss']
})
export class ModalComponent implements OnInit {
  @Input() mpathname: string;
  @ViewChild('content') content: ElementRef;
  // @ViewChild('btnClose') btnClose: ElementRef;
  // VIEW CHILD SHARING & didn't work
  // @ViewChild(TableComponent)
  // OUTPUT EMITTER & didn't work
  // @Output() public appendTimezone = new EventEmitter();

  closeResult: string;
  operation = 'create';
  model = 'timezone';
  model_name = '';
  index = 0;
  id: number;
  timezoneForm: FormGroup;
  userForm: FormGroup;
  submitted = false;
  timezone_utc: Array<any> = Array.from(
    {length: 48}, (v, i) => {
        if (i < 23) {
            return (i % 2 === 0) ? ((i - (12 + (i / 2))).toString() + ':00') : ((i - (11.5 + (i / 2))).toString() + ':30');
        } else {
            return (i % 2 === 0) ? ((i - (12 + (i / 2))).toString() + ':30') : ((i - (11.5 + (i / 2))).toString() + ':00');
        }
    });
  allGender = ['Male', 'Female', 'Other'];
  roles = [[1, 'User'], [2, 'Manager'], [3, 'Admin']];

  // private tableComponent: TableComponent;

  constructor(
    private modalService: NgbModal,
    private formBuilder: FormBuilder,
    private timezone: TimezoneService,
    private user: UserService,
    // private router: Router,
    // private table: TableComponent,
  ) {
      this.timezone.openModal.subscribe(info => {
        this.modalService.dismissAll();
        console.log('Edit Timezone Modal Called to open up');
        const data = info['data'];
        this.operation = <string>info['operation'];
        this.model = <string>info['model'];
        this.model_name = <string>data.name;
        this.index = <number>info['index'];
        this.submitted = false;
        this.timezoneForm = this.formBuilder.group({
          id: [data.id, Validators.required],
          index: [this.index, Validators.required],
          name: [data.name, Validators.required],
          city: [data.city, Validators.required],
          timezone: [data.utc, Validators.required],
        });
        this.id = data.id;
        this.modalService.open(this.content, { centered: true });
      });

      this.user.openModal.subscribe(info => {
        this.modalService.dismissAll();
        console.log('Edit User Modal Called to open up');
        const data = info['data'];
        this.operation = <string>info['operation'];
        this.model = <string>info['model'];
        this.model_name = <string>data.name;
        this.index = <number>info['index'];
        this.submitted = false;
        this.userForm = this.formBuilder.group({
          id: [data.id, Validators.required],
          index: [this.index, Validators.required],
          name: [data.name, Validators.required],
          email: [data.email, [
            Validators.required,
            Validators.pattern('[^ @]*@[^ @]*'),
            Validators.email,
          ]],
          username: [data.username, Validators.required],
          age: [data.age || 0, Validators.min(0)],
          gender: [data.gender],
          role_id: [data.role_id],
        });
        this.id = data.id;
        this.modalService.open(this.content, { centered: true });
      });
    }


  ngOnInit() {
    console.log('Modal called');
  }

  // ngAfterViewInit() {

  // }

  // convenience getter for easy access to form fields
  get timezone_form() {
    return this.timezoneForm.controls;
  }

  get user_form() {
    return this.userForm.controls;
  }
  // Submit Form
  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.mpathname === 'timezones'){
      if (this.timezoneForm.invalid) {
        console.log('Timezone Form Invalid Errors');
        return;
      } else {
        const params = {};
        this.timezoneForm.value['utc'] = this.timezoneForm.value.timezone;
        Object.entries(this.timezoneForm.value).forEach(
          ([key, value]) => {
            params[`timezone[${key}]`] = value;
          }
        );
        if (this.operation === 'create') {
          this.timezone.create(params).subscribe(
            data => {
              console.log('Data', data);
              // Event Emitter & Didn't work
              this.timezone.append_data(data);

              // PATCH FOR NOW & didn't work
              // this.router.navigate(['timezones']);

              // Calling Table component to append timezone & didn't work
              // this.table.append_timezone();

              // this.timezoneForm.reset();
              this.modalService.dismissAll();

              alert('Timezone Created Successfully');
            },
            error => {
              console.log('Error', error);
              alert('Error Occured \n Please try again later!!!');
            }
          );
        } else {
          console.log('Performing Timezone Edit');
          console.log(params);
          this.timezone.update(this.id, params).subscribe(
            data => {
              console.log('Data', data);
              // Event Emitter & Didn't work
              data['index'] = this.index;
              this.timezone.append_data(data);

              // PATCH FOR NOW & didn't work
              // this.router.navigate(['timezones']);

              // Calling Table component to append timezone & didn't work
              // this.table.append_timezone();

              // this.timezoneForm.reset();
              this.modalService.dismissAll();

              alert('Timezone Updated Successfully');
            },
            error => {
              console.log('Error', error);
              alert('Error Occured \n Please try again later!!!');
            }
          );
        }
      }
    }

    if (this.mpathname === 'users') {
      if (this.userForm.invalid) {
        console.log('Errors in User Form');
        return;
      } else {
        const params = {};
        Object.entries(this.userForm.value).forEach(
          ([key, value]) => {
            if (value || value <= 0) {
              params[`user[${key}]`] = value;
            }

          }
        );
        if (this.operation === 'create') {
          this.user.create(params).subscribe(
            data => {
              console.log('Data', data);
              // Event Emitter & Didn't work
              this.user.appendData(data);

              // PATCH FOR NOW & didn't work
              // this.router.navigate(['timezones']);

              // Calling Table component to append timezone & didn't work
              // this.table.append_timezone();

              // this.timezoneForm.reset();
              this.modalService.dismissAll();
              this.submitted = false;
              alert('User Created Successfully');
            },
            error => {
              console.log('Error', error);
              if (error.status <= 500) {
                alert('Error Occured \n' + error.error.Error);
              } else {
                alert('Error Occured \n Please try again later!!!');
              }
            }
          );
        } else {
          console.log('Performing User Edit');
          console.log(params);
          this.user.update(this.id, params).subscribe(
            data => {
              console.log('Data', data);
              // Event Emitter & Didn't work
              data['index'] = this.index;
              this.user.appendData(data);

              // PATCH FOR NOW & didn't work
              // this.router.navigate(['timezones']);

              // Calling Table component to append timezone & didn't work
              // this.table.append_timezone();

              // this.timezoneForm.reset();
              this.modalService.dismissAll();
              this.submitted = false;
              alert('User Updated Successfully');
            },
            error => {
              console.log('Error', error);
              if (error.status <= 500) {
                alert('Error Occured \n' + error.error.Error);
              } else {
                alert('Error Occured \n Please try again later!!!');
              }
            }
          );
        }
      }
    }
  }


  // To open Modal
  openVerticallyCentered(content) {
    this.modalService.dismissAll();
    this.operation = 'create';
    this.model_name = '';
    this.submitted = false;
    console.log('Content', content);
    if (this.mpathname === 'timezones') {
      this.model = 'timezone';
      this.timezoneForm = this.formBuilder.group({
        name: ['', Validators.required],
        city: ['', Validators.required],
        timezone: ['', Validators.required],
      });
    }
    if (this.mpathname === 'users') {
      this.model = 'user';
      this.userForm = this.formBuilder.group({
        name: ['', Validators.required],
        email: ['', [
          Validators.required,
          Validators.pattern('[^ @]*@[^ @]*'),
          Validators.email,
        ]],
        username: ['', Validators.required],
        age: [0],
        gender: [''],
        role_id: [1],
      });
    }
    this.modalService.open(content, { centered: true });
  }
}
