import { Component, OnInit, Input, Output, EventEmitter, OnChanges } from '@angular/core';


@Component({
  selector: 'app-table-search',
  templateUrl: './table-search.component.html',
  styleUrls: ['./table-search.component.scss']
})
export class TableSearchComponent implements OnInit {
  // @Input() tspathname: string;
  searchTerm: string;
  @Output() public searchedTerm = new EventEmitter();
  @Output() public searchClicked = new EventEmitter();
  constructor() { }

  ngOnInit() {
    console.log('Table Search called');
  }

  searchChange (input: string) {
    console.log(this.searchTerm);
    this.searchedTerm.emit(this.searchTerm);
  }

  searchClick(input: string) {
    console.log('Search Clicked!!!');
    this.searchClicked.emit(this.searchTerm);
    // this.searchTerm = null;
  }
}
