import { Component, OnInit, ViewEncapsulation, Output } from '@angular/core';
// import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Location } from '@angular/common';
import { EventEmitter } from 'protractor';
import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'app-right-panel',
  templateUrl: './right-panel.component.html',
  styleUrls: ['./right-panel.component.scss']
})
export class RightPanelComponent implements OnInit {
  pathname = 'timezone';
  user$: Object;
  timezone_catcher: any;
  // @Output() openModal = new EventEmitter();

  constructor(
    public location: Location,
    private router: Router,
    private cookieService: CookieService,
  ) { }

  ngOnInit() {
    console.log('Right Panel called');
    if (!this.cookieService.get('x-access-token')) {
      this.router.navigate(['']);
    }
    this.pathname = location.pathname.replace( /\/session/g, '' ).replace(/\//g, '');
    this.pathname = this.pathname ? this.pathname : 'timezones';
    console.log(this.pathname);
  }

  // openVerticallyCentered(content) {
  //   console.log('I am here');
  //   // this.openModal.emit('clicked');
  //   // this.modalService.open(content, { centered: true });
  // }

}
