import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-left-panel',
  templateUrl: './left-panel.component.html',
  styleUrls: ['./left-panel.component.scss']
})
export class LeftPanelComponent implements OnInit {
  timezone_active: string;
  user_active: string;
  constructor() { }

  ngOnInit() {
    console.log('Left Panel Called');
    this.timezone_active = 'active';
    this.user_active = '';
  }

  user_activated() {
    this.timezone_active = '';
    this.user_active = 'active';
  }

  timezone_activated() {
    this.timezone_active = 'active';
    this.user_active = '';
  }
}
