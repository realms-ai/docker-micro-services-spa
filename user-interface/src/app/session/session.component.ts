import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { EventEmitter } from 'protractor';

@Component({
  selector: 'app-session',
  templateUrl: './session.component.html',
  styleUrls: ['./session.component.scss']
})
export class SessionComponent implements OnInit {
  pathname = 'timezone';
  constructor(public location: Location) { }

  ngOnInit() {
    console.log('Session called');
    this.pathname = location.pathname.replace( /\/session\//g, '' );
    console.log(this.pathname);
  }

}
