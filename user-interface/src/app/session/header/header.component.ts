import { throwError } from 'rxjs';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';

// IMPORT SERVICES
import { UserSessionService } from './../../authentication/services/user-session.service';


@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  showClass = 'hide';
  constructor(
    private userSesssion: UserSessionService,
    private router: Router,
    private cookieService: CookieService,
  ) { }

  ngOnInit() {
    console.log('Header Called');
  }

  toggleDropdown() {
    this.showClass = this.showClass === 'show' ? 'hide' : 'show';
  }

  logout() {
    this.userSesssion.logout().subscribe(
      data => {
        console.log('User logout');
        this.cookieService.delete( 'x-access-token' );
        setTimeout(() => {
          this.router.navigate(['']);
        }, 1000);
      },
      error => {
        console.log('Error', error);
        alert('Error Occured \n Please try again later!!!');
      }
    )
  }
}
