import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse, HttpParams } from '@angular/common/http';
import { Observable, throwError, Subject, BehaviorSubject, of } from 'rxjs';
import { catchError, retry, take } from 'rxjs/operators';
import { CookieService } from 'ngx-cookie-service';
// import { ConsoleReporter } from 'jasmine';
import { environment } from './../../../environments/environment';


@Injectable({
  providedIn: 'root'
})
export class UserSessionService {
  headers: any;
  url: string;
  public accessToken = new Subject<String>();
  public event = this.accessToken.asObservable();

  constructor(
    private http: HttpClient,
    private cookieService: CookieService,
  ) {
    this.headers = {
      headers: new HttpHeaders({
        'Content-Type': 'application/x-www-form-urlencoded',
        'Accept': 'application/json'
      })
    };
    this.url = environment.user_session;
  }

  signup(params) {
    const signupParams = new HttpParams({fromObject: params});
    console.log(signupParams);
    return this.http.post(this.url + 'signup', signupParams, this.headers);
  }

  login(params) {
    const loginParams = new HttpParams({fromObject: params});
    console.log(loginParams);
    return this.http.post(this.url + 'login', loginParams, this.headers);
  }

  reset_password(params) {
    return this.http.post(this.url + 'reset', params, this.headers);
  }

  forgot_password(params) {
    return this.http.post(this.url + 'forgot', params, this.headers);
  }

  logout() {
    this.headers = {
      headers: new HttpHeaders({
        'x-access-token': this.cookieService.get('x-access-token'),
        'Content-Type': 'application/x-www-form-urlencoded',
        'Accept': 'application/json'
      })
    };
    return this.http.get(this.url + 'logout', this.headers);
  }

  authToken(data: String) {
    // Emitting Observable but didn't work
    this.accessToken.next(data);
  }

  private handleError(error: HttpErrorResponse) {
    console.error('An error occurred in User Session Services');
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${JSON.stringify(error.error)}`);
    }
    // return an observable with a user-facing error message
    return throwError(
      'Something bad happened; please try again later.');
  }
}
