import { MatchPasswordValidator } from './../../shared/match-password.directive';
import { ForgotPasswordComponent } from './../forgot-password/forgot-password.component';
import { Component, OnInit, Input, AfterViewInit, ViewChild, Output, EventEmitter, ElementRef, TemplateRef } from '@angular/core';
import { Observable } from 'rxjs';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';


// IMPORTING SERVICES
import { UserSessionService } from './../services/user-session.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {
  submitted = false;
  signupForm: FormGroup;
  passwordForm: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private userSession: UserSessionService,
    private cookieService: CookieService,
    private router: Router,
  ) { }

  ngOnInit() {
    console.log('Login Component');
    const matching_field = 'password';
    this.passwordForm = this.formBuilder.group(
      {
        password: ['', Validators.required],
        confirm_password: ['', [Validators.required]],
      },
      {
        validator: MatchPasswordValidator    // Match all values in passwords group
      });
    this.signupForm = this.formBuilder.group({
      name: ['', Validators.required],
      username: ['', Validators.required],
      email: ['', [
        Validators.required,
        Validators.pattern('[^ @]*@[^ @]*'),
        Validators.email,
      ]],
      passwords: this.passwordForm,
    });
    if (this.cookieService.get('x-access-token')) {
      this.router.navigate(['session']);
    }
  }

  // convenience getter for easy access to form fields
  get sf() {
    return this.signupForm.controls;
  }

  get pf() {
    return this.passwordForm.controls;
  }

  // Submit Form
  onSubmit() {
    this.submitted = true;
    // CUSTOM CHECK don't work, IMPLEMENT CUSTOM VALIDATOR
    // if (this.signupForm.value.password !== this.signupForm.value.confirm_password){
    //   password_match = false;
    //   this.sf.confirm_password.hasError = {match: 'Password & confirm password doesn\'t match!!!'}
    //   this.sf.confirm_password.errors = {match: 'Password & confirm password doesn\'t match!!!'}
    // }
    if (this.signupForm.invalid) {
      console.log('Signup Form Invalid Errors');
      return;
    } else {
      const params = {};
      Object.entries(this.signupForm.value).forEach(
        ([key, value]) => {
          params[`user[${key}]`] = value;
        }
      );
      Object.entries(this.passwordForm.value).forEach(
        ([key, value]) => {
          params[`user[${key}]`] = value;
        }
      );
      this.userSession.signup(params).subscribe(
        data => {
          alert('Signup Successful!!!');
          // Redirect to Login route
          this.router.navigate(['']);
        },
        error => {
          console.log('Error', error);
          if (error.status === 422) {
            alert('Error Occured \n' + JSON.stringify(error.error));
          } else {
            alert('Error Occured \n Please try again later!!!');
          }
        }
      );
    }
  }

}
