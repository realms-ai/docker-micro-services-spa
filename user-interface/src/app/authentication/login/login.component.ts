import { Component, OnInit, Input, AfterViewInit, ViewChild, Output, EventEmitter, ElementRef, TemplateRef } from '@angular/core';
import { Observable } from 'rxjs';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';

// IMPORTING SERVICES
import { UserSessionService } from './../services/user-session.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  submitted = false;
  loginForm: FormGroup;
  authToken: string;

  constructor(
    private formBuilder: FormBuilder,
    private userSession: UserSessionService,
    private cookieService: CookieService,
    private router: Router,
  ) { }

  ngOnInit() {
    console.log('Login Component Called');
    this.loginForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required],
    });
    if (this.cookieService.get('x-access-token')) {
      this.router.navigate(['session']);
    }
  }

  // convenience getter for easy access to form fields
  get lf() {
    return this.loginForm.controls;
  }

  // Submit Form
  onSubmit() {
    this.submitted = true;
    if (this.loginForm.invalid) {
      console.log('Login Form Invalid Errors');
      return;
    } else {
      const params = {};
      Object.entries(this.loginForm.value).forEach(
        ([key, value]) => {
          params[`user[${key}]`] = value;
        }
      );
      this.userSession.login(params).subscribe(
        data => {
          this.authToken = data['jwt_token'];
          // storage of authToken to Cookies
          this.cookieService.set( 'x-access-token', this.authToken );
          // Redirect to Session route
          this.router.navigate(['session']);
        },
        error => {
          console.log('Error', error);
          if (error.status <= 500) {
            alert('Username & Password doesn\'t match!!!');
          } else {
            alert('Error Occured \n Please try again later!!!');
          }
        }
      );
    }
  }

}
