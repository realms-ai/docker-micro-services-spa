import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'nameSearch'
})
export class NameSearchPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    // console.log('I am in pipe');
    console.log('Value: ', value);
    console.log('Args: ', args);
    if (!value || !args) {
      return value;
    }
    console.log('I am in pipe');
    return value.filter(v => v.name.toLowerCase().indexOf(args.toLowerCase()) !== -1);
  }

}
