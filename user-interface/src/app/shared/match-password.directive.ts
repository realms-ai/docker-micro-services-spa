import { Directive, forwardRef, Attribute } from '@angular/core';
import { Validator, AbstractControl, NG_VALIDATORS, ValidatorFn, FormControl } from '@angular/forms';

// @Directive({
//   selector: '[appMatchPassword],[validatePassword][formControlName],[validatePassword][formControl],[validatePassword][ngModel]',
//   providers: [
//     { provide: NG_VALIDATORS, useExisting: forwardRef(() => MatchPasswordValidator), multi: true }
// ]
// })


export function MatchPasswordValidator(control: AbstractControl): { [key: string]: boolean } | null {
    const values = Object.values(control.value);
    // Rather than Using Object, user Map (https://codecraft.tv/courses/angular/es6-typescript/mapset/)
    const hash = new Map();

    values.forEach((v) => {
      if (v) {
        if (!hash.get(v)) {
          hash.set(v, []);
        }
        hash.get(v).push(v);
      }
    });

    // Rather than using Array, use SET to get keys and values of object
    const uniqueArray = new Set(hash.keys());
    // console.log('Hash: ', hash);
    // console.log('uniqueArray: ', uniqueArray);
    // console.log('UA Length: ', uniqueArray.size);
    if (uniqueArray.size > 1 && values.length > 1) {
      // console.log('Error returned unmatch');
      return { 'unmatch': true };
    }
    return null;
}

