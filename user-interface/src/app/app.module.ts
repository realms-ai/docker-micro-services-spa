// IMPORTS
import { HttpClient } from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AppRoutingModule } from './app-routing.module';

// PROVIDERS
import { CookieService } from 'ngx-cookie-service';

// ALL DECLARATIONS
                // COMPONENTS
import { AppComponent } from './app.component';
import { LeftPanelComponent } from './session/left-panel/left-panel.component';
import { HeaderComponent } from './session/header/header.component';
import { RightPanelComponent } from './session/right-panel/right-panel.component';
import { BreadcrumbsComponent } from './session/right-panel/breadcrumbs/breadcrumbs.component';
import { TableSearchComponent } from './session/right-panel/table-search/table-search.component';
import { TableComponent } from './session/right-panel/table/table.component';
import { PaginationComponent } from './session/right-panel/pagination/pagination.component';
import { ModalComponent } from './session/right-panel/modal/modal.component';
import { AuthenticationComponent } from './authentication/authentication.component';
import { ForgotPasswordComponent } from './authentication/forgot-password/forgot-password.component';
import { ResetPasswordComponent } from './authentication/reset-password/reset-password.component';
import { LoginComponent } from './authentication/login/login.component';
import { SignupComponent } from './authentication/signup/signup.component';
import { SessionComponent } from './session/session.component';
                // DIRECTIVES
// Not included as needed in specific files
import { MatchPasswordValidator } from './shared/match-password.directive';
import { NameSearchPipe } from './shared/name-search.pipe';




@NgModule({
  declarations: [
    AppComponent,
    LeftPanelComponent,
    HeaderComponent,
    RightPanelComponent,
    BreadcrumbsComponent,
    TableSearchComponent,
    TableComponent,
    PaginationComponent,
    ModalComponent,
    AuthenticationComponent,
    ForgotPasswordComponent,
    ResetPasswordComponent,
    LoginComponent,
    SignupComponent,
    SessionComponent,
    NameSearchPipe,
    // MatchPasswordValidator    (Not included as work as CUSTOM VALIDATOR)
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    NgbModule,
    ReactiveFormsModule,
    FormsModule,
  ],
  providers: [
    CookieService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
