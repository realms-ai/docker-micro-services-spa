# Sample test in 100 working hours
This test has been performed in micro services using different languages.
Details of the project are in [wiki](https://bitbucket.org/navpreet_singh_21/docker-micro-services-spa/wiki/Home)

1. [**Users**](https://bitbucket.org/navpreet_singh_21/docker-micro-services-spa/src/master/users/) folder is a micro-service written in **Python** on **Flask** framework
2. [**User Session**](https://bitbucket.org/navpreet_singh_21/docker-micro-services-spa/src/master/user_session/) folder is a micro-service written in **Ruby** on **Rails** framework
3. [**Timezone**](https://bitbucket.org/navpreet_singh_21/docker-micro-services-spa/src/master/timezones/) folder is a micro-service written in **Node.Js** on **Express** framework in **Typescript** language
4. [**User Interface**](https://bitbucket.org/navpreet_singh_21/docker-micro-services-spa/src/master/user-interface/) is a front end application written in **Angular 6**